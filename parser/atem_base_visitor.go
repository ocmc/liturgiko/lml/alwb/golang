// Code generated from java-escape by ANTLR 4.11.1. DO NOT EDIT.

package parser // Atem

import "github.com/antlr/antlr4/runtime/Go/antlr/v4"

type BaseAtemVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseAtemVisitor) VisitAtemModel(ctx *AtemModelContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHead(ctx *HeadContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeadComponent(ctx *HeadComponentContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitCommemoration(ctx *CommemorationContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitPreface(ctx *PrefaceContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitPrefaceElementType(ctx *PrefaceElementTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterFragment(ctx *HeaderFooterFragmentContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterText(ctx *HeaderFooterTextContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterDate(ctx *HeaderFooterDateContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterPageNumber(ctx *HeaderFooterPageNumberContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterLookup(ctx *HeaderFooterLookupContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterTitle(ctx *HeaderFooterTitleContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterCommemoration(ctx *HeaderFooterCommemorationContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitSetDate(ctx *SetDateContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitMcDay(ctx *McDayContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitTemplateTitle(ctx *TemplateTitleContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitPageKeepWithNext(ctx *PageKeepWithNextContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitPageHeaderEven(ctx *PageHeaderEvenContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitPageHeaderOdd(ctx *PageHeaderOddContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitPageFooterEven(ctx *PageFooterEvenContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitPageFooterOdd(ctx *PageFooterOddContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterColumn(ctx *HeaderFooterColumnContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterColumnLeft(ctx *HeaderFooterColumnLeftContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterColumnCenter(ctx *HeaderFooterColumnCenterContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHeaderFooterColumnRight(ctx *HeaderFooterColumnRightContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitPageNumber(ctx *PageNumberContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitAbstractComponent(ctx *AbstractComponentContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitActor(ctx *ActorContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitBlock(ctx *BlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitDialog(ctx *DialogContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitHymn(ctx *HymnContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitMedia(ctx *MediaContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitParagraph(ctx *ParagraphContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitPassThroughHtml(ctx *PassThroughHtmlContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitReading(ctx *ReadingContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitRestoreLocale(ctx *RestoreLocaleContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitRubric(ctx *RubricContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitSection(ctx *SectionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitSetLocale(ctx *SetLocaleContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitSubTitle(ctx *SubTitleContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitTitle(ctx *TitleContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitVerse(ctx *VerseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitVersion(ctx *VersionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitVersionSwitch(ctx *VersionSwitchContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitVersionSwitchType(ctx *VersionSwitchTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenDate(ctx *WhenDateContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenDateCase(ctx *WhenDateCaseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenOther(ctx *WhenOtherContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenPeriodCase(ctx *WhenPeriodCaseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitAbstractDayCase(ctx *AbstractDayCaseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitDayRange(ctx *DayRangeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitDaySet(ctx *DaySetContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenMovableCycleDay(ctx *WhenMovableCycleDayContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitAbstractDateCase(ctx *AbstractDateCaseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitDateRange(ctx *DateRangeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitDateSet(ctx *DateSetContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenDayName(ctx *WhenDayNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenDayNameCase(ctx *WhenDayNameCaseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitAbstractDayNameCase(ctx *AbstractDayNameCaseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitDayNameRange(ctx *DayNameRangeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitDayNameSet(ctx *DayNameSetContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenExists(ctx *WhenExistsContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenExistsCase(ctx *WhenExistsCaseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenModeOfWeek(ctx *WhenModeOfWeekContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenModeOfWeekCase(ctx *WhenModeOfWeekCaseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitModeOfWeekSet(ctx *ModeOfWeekSetContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenLukanCycleDay(ctx *WhenLukanCycleDayContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenSundayAfterElevationOfCrossDay(ctx *WhenSundayAfterElevationOfCrossDayContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitWhenSundaysBeforeTriodion(ctx *WhenSundaysBeforeTriodionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitSundaysBeforeTriodionCase(ctx *SundaysBeforeTriodionCaseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitDayOverride(ctx *DayOverrideContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitElementType(ctx *ElementTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitResourceText(ctx *ResourceTextContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitTaggedText(ctx *TaggedTextContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitLookup(ctx *LookupContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitLdp(ctx *LdpContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitInsertBreak(ctx *InsertBreakContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitImportBlock(ctx *ImportBlockContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitModeOverride(ctx *ModeOverrideContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitRole(ctx *RoleContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitSectionFragment(ctx *SectionFragmentContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitTemplateFragment(ctx *TemplateFragmentContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitTemplateStatus(ctx *TemplateStatusContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitQualifiedName(ctx *QualifiedNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitQualifiedNameWithWildCard(ctx *QualifiedNameWithWildCardContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitLdpType(ctx *LdpTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitBreakType(ctx *BreakTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitLanguage(ctx *LanguageContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitDayOfWeek(ctx *DayOfWeekContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitModeTypes(ctx *ModeTypesContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitMonthName(ctx *MonthNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseAtemVisitor) VisitTemplateStatuses(ctx *TemplateStatusesContext) interface{} {
	return v.VisitChildren(ctx)
}
