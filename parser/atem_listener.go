// Code generated from java-escape by ANTLR 4.11.1. DO NOT EDIT.

package parser // Atem

import "github.com/antlr/antlr4/runtime/Go/antlr/v4"

// AtemListener is a complete listener for a parse tree produced by AtemParser.
type AtemListener interface {
	antlr.ParseTreeListener

	// EnterAtemModel is called when entering the atemModel production.
	EnterAtemModel(c *AtemModelContext)

	// EnterHead is called when entering the head production.
	EnterHead(c *HeadContext)

	// EnterHeadComponent is called when entering the headComponent production.
	EnterHeadComponent(c *HeadComponentContext)

	// EnterCommemoration is called when entering the commemoration production.
	EnterCommemoration(c *CommemorationContext)

	// EnterPreface is called when entering the preface production.
	EnterPreface(c *PrefaceContext)

	// EnterPrefaceElementType is called when entering the prefaceElementType production.
	EnterPrefaceElementType(c *PrefaceElementTypeContext)

	// EnterHeaderFooterFragment is called when entering the headerFooterFragment production.
	EnterHeaderFooterFragment(c *HeaderFooterFragmentContext)

	// EnterHeaderFooterText is called when entering the headerFooterText production.
	EnterHeaderFooterText(c *HeaderFooterTextContext)

	// EnterHeaderFooterDate is called when entering the headerFooterDate production.
	EnterHeaderFooterDate(c *HeaderFooterDateContext)

	// EnterHeaderFooterPageNumber is called when entering the headerFooterPageNumber production.
	EnterHeaderFooterPageNumber(c *HeaderFooterPageNumberContext)

	// EnterHeaderFooterLookup is called when entering the headerFooterLookup production.
	EnterHeaderFooterLookup(c *HeaderFooterLookupContext)

	// EnterHeaderFooterTitle is called when entering the headerFooterTitle production.
	EnterHeaderFooterTitle(c *HeaderFooterTitleContext)

	// EnterHeaderFooterCommemoration is called when entering the headerFooterCommemoration production.
	EnterHeaderFooterCommemoration(c *HeaderFooterCommemorationContext)

	// EnterSetDate is called when entering the setDate production.
	EnterSetDate(c *SetDateContext)

	// EnterMcDay is called when entering the mcDay production.
	EnterMcDay(c *McDayContext)

	// EnterTemplateTitle is called when entering the templateTitle production.
	EnterTemplateTitle(c *TemplateTitleContext)

	// EnterPageKeepWithNext is called when entering the pageKeepWithNext production.
	EnterPageKeepWithNext(c *PageKeepWithNextContext)

	// EnterPageHeaderEven is called when entering the pageHeaderEven production.
	EnterPageHeaderEven(c *PageHeaderEvenContext)

	// EnterPageHeaderOdd is called when entering the pageHeaderOdd production.
	EnterPageHeaderOdd(c *PageHeaderOddContext)

	// EnterPageFooterEven is called when entering the pageFooterEven production.
	EnterPageFooterEven(c *PageFooterEvenContext)

	// EnterPageFooterOdd is called when entering the pageFooterOdd production.
	EnterPageFooterOdd(c *PageFooterOddContext)

	// EnterHeaderFooterColumn is called when entering the headerFooterColumn production.
	EnterHeaderFooterColumn(c *HeaderFooterColumnContext)

	// EnterHeaderFooterColumnLeft is called when entering the headerFooterColumnLeft production.
	EnterHeaderFooterColumnLeft(c *HeaderFooterColumnLeftContext)

	// EnterHeaderFooterColumnCenter is called when entering the headerFooterColumnCenter production.
	EnterHeaderFooterColumnCenter(c *HeaderFooterColumnCenterContext)

	// EnterHeaderFooterColumnRight is called when entering the headerFooterColumnRight production.
	EnterHeaderFooterColumnRight(c *HeaderFooterColumnRightContext)

	// EnterPageNumber is called when entering the pageNumber production.
	EnterPageNumber(c *PageNumberContext)

	// EnterAbstractComponent is called when entering the abstractComponent production.
	EnterAbstractComponent(c *AbstractComponentContext)

	// EnterActor is called when entering the actor production.
	EnterActor(c *ActorContext)

	// EnterBlock is called when entering the block production.
	EnterBlock(c *BlockContext)

	// EnterDialog is called when entering the dialog production.
	EnterDialog(c *DialogContext)

	// EnterHymn is called when entering the hymn production.
	EnterHymn(c *HymnContext)

	// EnterMedia is called when entering the media production.
	EnterMedia(c *MediaContext)

	// EnterParagraph is called when entering the paragraph production.
	EnterParagraph(c *ParagraphContext)

	// EnterPassThroughHtml is called when entering the passThroughHtml production.
	EnterPassThroughHtml(c *PassThroughHtmlContext)

	// EnterReading is called when entering the reading production.
	EnterReading(c *ReadingContext)

	// EnterRestoreLocale is called when entering the restoreLocale production.
	EnterRestoreLocale(c *RestoreLocaleContext)

	// EnterRubric is called when entering the rubric production.
	EnterRubric(c *RubricContext)

	// EnterSection is called when entering the section production.
	EnterSection(c *SectionContext)

	// EnterSetLocale is called when entering the setLocale production.
	EnterSetLocale(c *SetLocaleContext)

	// EnterSubTitle is called when entering the subTitle production.
	EnterSubTitle(c *SubTitleContext)

	// EnterTitle is called when entering the title production.
	EnterTitle(c *TitleContext)

	// EnterVerse is called when entering the verse production.
	EnterVerse(c *VerseContext)

	// EnterVersion is called when entering the version production.
	EnterVersion(c *VersionContext)

	// EnterVersionSwitch is called when entering the versionSwitch production.
	EnterVersionSwitch(c *VersionSwitchContext)

	// EnterVersionSwitchType is called when entering the versionSwitchType production.
	EnterVersionSwitchType(c *VersionSwitchTypeContext)

	// EnterWhenDate is called when entering the whenDate production.
	EnterWhenDate(c *WhenDateContext)

	// EnterWhenDateCase is called when entering the whenDateCase production.
	EnterWhenDateCase(c *WhenDateCaseContext)

	// EnterWhenOther is called when entering the whenOther production.
	EnterWhenOther(c *WhenOtherContext)

	// EnterWhenPeriodCase is called when entering the whenPeriodCase production.
	EnterWhenPeriodCase(c *WhenPeriodCaseContext)

	// EnterAbstractDayCase is called when entering the abstractDayCase production.
	EnterAbstractDayCase(c *AbstractDayCaseContext)

	// EnterDayRange is called when entering the dayRange production.
	EnterDayRange(c *DayRangeContext)

	// EnterDaySet is called when entering the daySet production.
	EnterDaySet(c *DaySetContext)

	// EnterWhenMovableCycleDay is called when entering the whenMovableCycleDay production.
	EnterWhenMovableCycleDay(c *WhenMovableCycleDayContext)

	// EnterAbstractDateCase is called when entering the abstractDateCase production.
	EnterAbstractDateCase(c *AbstractDateCaseContext)

	// EnterDateRange is called when entering the dateRange production.
	EnterDateRange(c *DateRangeContext)

	// EnterDateSet is called when entering the dateSet production.
	EnterDateSet(c *DateSetContext)

	// EnterWhenDayName is called when entering the whenDayName production.
	EnterWhenDayName(c *WhenDayNameContext)

	// EnterWhenDayNameCase is called when entering the whenDayNameCase production.
	EnterWhenDayNameCase(c *WhenDayNameCaseContext)

	// EnterAbstractDayNameCase is called when entering the abstractDayNameCase production.
	EnterAbstractDayNameCase(c *AbstractDayNameCaseContext)

	// EnterDayNameRange is called when entering the dayNameRange production.
	EnterDayNameRange(c *DayNameRangeContext)

	// EnterDayNameSet is called when entering the dayNameSet production.
	EnterDayNameSet(c *DayNameSetContext)

	// EnterWhenExists is called when entering the whenExists production.
	EnterWhenExists(c *WhenExistsContext)

	// EnterWhenExistsCase is called when entering the whenExistsCase production.
	EnterWhenExistsCase(c *WhenExistsCaseContext)

	// EnterWhenModeOfWeek is called when entering the whenModeOfWeek production.
	EnterWhenModeOfWeek(c *WhenModeOfWeekContext)

	// EnterWhenModeOfWeekCase is called when entering the whenModeOfWeekCase production.
	EnterWhenModeOfWeekCase(c *WhenModeOfWeekCaseContext)

	// EnterModeOfWeekSet is called when entering the modeOfWeekSet production.
	EnterModeOfWeekSet(c *ModeOfWeekSetContext)

	// EnterWhenLukanCycleDay is called when entering the whenLukanCycleDay production.
	EnterWhenLukanCycleDay(c *WhenLukanCycleDayContext)

	// EnterWhenSundayAfterElevationOfCrossDay is called when entering the whenSundayAfterElevationOfCrossDay production.
	EnterWhenSundayAfterElevationOfCrossDay(c *WhenSundayAfterElevationOfCrossDayContext)

	// EnterWhenSundaysBeforeTriodion is called when entering the whenSundaysBeforeTriodion production.
	EnterWhenSundaysBeforeTriodion(c *WhenSundaysBeforeTriodionContext)

	// EnterSundaysBeforeTriodionCase is called when entering the sundaysBeforeTriodionCase production.
	EnterSundaysBeforeTriodionCase(c *SundaysBeforeTriodionCaseContext)

	// EnterDayOverride is called when entering the dayOverride production.
	EnterDayOverride(c *DayOverrideContext)

	// EnterElementType is called when entering the elementType production.
	EnterElementType(c *ElementTypeContext)

	// EnterResourceText is called when entering the resourceText production.
	EnterResourceText(c *ResourceTextContext)

	// EnterTaggedText is called when entering the taggedText production.
	EnterTaggedText(c *TaggedTextContext)

	// EnterLookup is called when entering the lookup production.
	EnterLookup(c *LookupContext)

	// EnterLdp is called when entering the ldp production.
	EnterLdp(c *LdpContext)

	// EnterInsertBreak is called when entering the insertBreak production.
	EnterInsertBreak(c *InsertBreakContext)

	// EnterImportBlock is called when entering the importBlock production.
	EnterImportBlock(c *ImportBlockContext)

	// EnterModeOverride is called when entering the modeOverride production.
	EnterModeOverride(c *ModeOverrideContext)

	// EnterRole is called when entering the role production.
	EnterRole(c *RoleContext)

	// EnterSectionFragment is called when entering the sectionFragment production.
	EnterSectionFragment(c *SectionFragmentContext)

	// EnterTemplateFragment is called when entering the templateFragment production.
	EnterTemplateFragment(c *TemplateFragmentContext)

	// EnterTemplateStatus is called when entering the templateStatus production.
	EnterTemplateStatus(c *TemplateStatusContext)

	// EnterQualifiedName is called when entering the qualifiedName production.
	EnterQualifiedName(c *QualifiedNameContext)

	// EnterQualifiedNameWithWildCard is called when entering the qualifiedNameWithWildCard production.
	EnterQualifiedNameWithWildCard(c *QualifiedNameWithWildCardContext)

	// EnterLdpType is called when entering the ldpType production.
	EnterLdpType(c *LdpTypeContext)

	// EnterBreakType is called when entering the breakType production.
	EnterBreakType(c *BreakTypeContext)

	// EnterLanguage is called when entering the language production.
	EnterLanguage(c *LanguageContext)

	// EnterDayOfWeek is called when entering the dayOfWeek production.
	EnterDayOfWeek(c *DayOfWeekContext)

	// EnterModeTypes is called when entering the modeTypes production.
	EnterModeTypes(c *ModeTypesContext)

	// EnterMonthName is called when entering the monthName production.
	EnterMonthName(c *MonthNameContext)

	// EnterTemplateStatuses is called when entering the templateStatuses production.
	EnterTemplateStatuses(c *TemplateStatusesContext)

	// ExitAtemModel is called when exiting the atemModel production.
	ExitAtemModel(c *AtemModelContext)

	// ExitHead is called when exiting the head production.
	ExitHead(c *HeadContext)

	// ExitHeadComponent is called when exiting the headComponent production.
	ExitHeadComponent(c *HeadComponentContext)

	// ExitCommemoration is called when exiting the commemoration production.
	ExitCommemoration(c *CommemorationContext)

	// ExitPreface is called when exiting the preface production.
	ExitPreface(c *PrefaceContext)

	// ExitPrefaceElementType is called when exiting the prefaceElementType production.
	ExitPrefaceElementType(c *PrefaceElementTypeContext)

	// ExitHeaderFooterFragment is called when exiting the headerFooterFragment production.
	ExitHeaderFooterFragment(c *HeaderFooterFragmentContext)

	// ExitHeaderFooterText is called when exiting the headerFooterText production.
	ExitHeaderFooterText(c *HeaderFooterTextContext)

	// ExitHeaderFooterDate is called when exiting the headerFooterDate production.
	ExitHeaderFooterDate(c *HeaderFooterDateContext)

	// ExitHeaderFooterPageNumber is called when exiting the headerFooterPageNumber production.
	ExitHeaderFooterPageNumber(c *HeaderFooterPageNumberContext)

	// ExitHeaderFooterLookup is called when exiting the headerFooterLookup production.
	ExitHeaderFooterLookup(c *HeaderFooterLookupContext)

	// ExitHeaderFooterTitle is called when exiting the headerFooterTitle production.
	ExitHeaderFooterTitle(c *HeaderFooterTitleContext)

	// ExitHeaderFooterCommemoration is called when exiting the headerFooterCommemoration production.
	ExitHeaderFooterCommemoration(c *HeaderFooterCommemorationContext)

	// ExitSetDate is called when exiting the setDate production.
	ExitSetDate(c *SetDateContext)

	// ExitMcDay is called when exiting the mcDay production.
	ExitMcDay(c *McDayContext)

	// ExitTemplateTitle is called when exiting the templateTitle production.
	ExitTemplateTitle(c *TemplateTitleContext)

	// ExitPageKeepWithNext is called when exiting the pageKeepWithNext production.
	ExitPageKeepWithNext(c *PageKeepWithNextContext)

	// ExitPageHeaderEven is called when exiting the pageHeaderEven production.
	ExitPageHeaderEven(c *PageHeaderEvenContext)

	// ExitPageHeaderOdd is called when exiting the pageHeaderOdd production.
	ExitPageHeaderOdd(c *PageHeaderOddContext)

	// ExitPageFooterEven is called when exiting the pageFooterEven production.
	ExitPageFooterEven(c *PageFooterEvenContext)

	// ExitPageFooterOdd is called when exiting the pageFooterOdd production.
	ExitPageFooterOdd(c *PageFooterOddContext)

	// ExitHeaderFooterColumn is called when exiting the headerFooterColumn production.
	ExitHeaderFooterColumn(c *HeaderFooterColumnContext)

	// ExitHeaderFooterColumnLeft is called when exiting the headerFooterColumnLeft production.
	ExitHeaderFooterColumnLeft(c *HeaderFooterColumnLeftContext)

	// ExitHeaderFooterColumnCenter is called when exiting the headerFooterColumnCenter production.
	ExitHeaderFooterColumnCenter(c *HeaderFooterColumnCenterContext)

	// ExitHeaderFooterColumnRight is called when exiting the headerFooterColumnRight production.
	ExitHeaderFooterColumnRight(c *HeaderFooterColumnRightContext)

	// ExitPageNumber is called when exiting the pageNumber production.
	ExitPageNumber(c *PageNumberContext)

	// ExitAbstractComponent is called when exiting the abstractComponent production.
	ExitAbstractComponent(c *AbstractComponentContext)

	// ExitActor is called when exiting the actor production.
	ExitActor(c *ActorContext)

	// ExitBlock is called when exiting the block production.
	ExitBlock(c *BlockContext)

	// ExitDialog is called when exiting the dialog production.
	ExitDialog(c *DialogContext)

	// ExitHymn is called when exiting the hymn production.
	ExitHymn(c *HymnContext)

	// ExitMedia is called when exiting the media production.
	ExitMedia(c *MediaContext)

	// ExitParagraph is called when exiting the paragraph production.
	ExitParagraph(c *ParagraphContext)

	// ExitPassThroughHtml is called when exiting the passThroughHtml production.
	ExitPassThroughHtml(c *PassThroughHtmlContext)

	// ExitReading is called when exiting the reading production.
	ExitReading(c *ReadingContext)

	// ExitRestoreLocale is called when exiting the restoreLocale production.
	ExitRestoreLocale(c *RestoreLocaleContext)

	// ExitRubric is called when exiting the rubric production.
	ExitRubric(c *RubricContext)

	// ExitSection is called when exiting the section production.
	ExitSection(c *SectionContext)

	// ExitSetLocale is called when exiting the setLocale production.
	ExitSetLocale(c *SetLocaleContext)

	// ExitSubTitle is called when exiting the subTitle production.
	ExitSubTitle(c *SubTitleContext)

	// ExitTitle is called when exiting the title production.
	ExitTitle(c *TitleContext)

	// ExitVerse is called when exiting the verse production.
	ExitVerse(c *VerseContext)

	// ExitVersion is called when exiting the version production.
	ExitVersion(c *VersionContext)

	// ExitVersionSwitch is called when exiting the versionSwitch production.
	ExitVersionSwitch(c *VersionSwitchContext)

	// ExitVersionSwitchType is called when exiting the versionSwitchType production.
	ExitVersionSwitchType(c *VersionSwitchTypeContext)

	// ExitWhenDate is called when exiting the whenDate production.
	ExitWhenDate(c *WhenDateContext)

	// ExitWhenDateCase is called when exiting the whenDateCase production.
	ExitWhenDateCase(c *WhenDateCaseContext)

	// ExitWhenOther is called when exiting the whenOther production.
	ExitWhenOther(c *WhenOtherContext)

	// ExitWhenPeriodCase is called when exiting the whenPeriodCase production.
	ExitWhenPeriodCase(c *WhenPeriodCaseContext)

	// ExitAbstractDayCase is called when exiting the abstractDayCase production.
	ExitAbstractDayCase(c *AbstractDayCaseContext)

	// ExitDayRange is called when exiting the dayRange production.
	ExitDayRange(c *DayRangeContext)

	// ExitDaySet is called when exiting the daySet production.
	ExitDaySet(c *DaySetContext)

	// ExitWhenMovableCycleDay is called when exiting the whenMovableCycleDay production.
	ExitWhenMovableCycleDay(c *WhenMovableCycleDayContext)

	// ExitAbstractDateCase is called when exiting the abstractDateCase production.
	ExitAbstractDateCase(c *AbstractDateCaseContext)

	// ExitDateRange is called when exiting the dateRange production.
	ExitDateRange(c *DateRangeContext)

	// ExitDateSet is called when exiting the dateSet production.
	ExitDateSet(c *DateSetContext)

	// ExitWhenDayName is called when exiting the whenDayName production.
	ExitWhenDayName(c *WhenDayNameContext)

	// ExitWhenDayNameCase is called when exiting the whenDayNameCase production.
	ExitWhenDayNameCase(c *WhenDayNameCaseContext)

	// ExitAbstractDayNameCase is called when exiting the abstractDayNameCase production.
	ExitAbstractDayNameCase(c *AbstractDayNameCaseContext)

	// ExitDayNameRange is called when exiting the dayNameRange production.
	ExitDayNameRange(c *DayNameRangeContext)

	// ExitDayNameSet is called when exiting the dayNameSet production.
	ExitDayNameSet(c *DayNameSetContext)

	// ExitWhenExists is called when exiting the whenExists production.
	ExitWhenExists(c *WhenExistsContext)

	// ExitWhenExistsCase is called when exiting the whenExistsCase production.
	ExitWhenExistsCase(c *WhenExistsCaseContext)

	// ExitWhenModeOfWeek is called when exiting the whenModeOfWeek production.
	ExitWhenModeOfWeek(c *WhenModeOfWeekContext)

	// ExitWhenModeOfWeekCase is called when exiting the whenModeOfWeekCase production.
	ExitWhenModeOfWeekCase(c *WhenModeOfWeekCaseContext)

	// ExitModeOfWeekSet is called when exiting the modeOfWeekSet production.
	ExitModeOfWeekSet(c *ModeOfWeekSetContext)

	// ExitWhenLukanCycleDay is called when exiting the whenLukanCycleDay production.
	ExitWhenLukanCycleDay(c *WhenLukanCycleDayContext)

	// ExitWhenSundayAfterElevationOfCrossDay is called when exiting the whenSundayAfterElevationOfCrossDay production.
	ExitWhenSundayAfterElevationOfCrossDay(c *WhenSundayAfterElevationOfCrossDayContext)

	// ExitWhenSundaysBeforeTriodion is called when exiting the whenSundaysBeforeTriodion production.
	ExitWhenSundaysBeforeTriodion(c *WhenSundaysBeforeTriodionContext)

	// ExitSundaysBeforeTriodionCase is called when exiting the sundaysBeforeTriodionCase production.
	ExitSundaysBeforeTriodionCase(c *SundaysBeforeTriodionCaseContext)

	// ExitDayOverride is called when exiting the dayOverride production.
	ExitDayOverride(c *DayOverrideContext)

	// ExitElementType is called when exiting the elementType production.
	ExitElementType(c *ElementTypeContext)

	// ExitResourceText is called when exiting the resourceText production.
	ExitResourceText(c *ResourceTextContext)

	// ExitTaggedText is called when exiting the taggedText production.
	ExitTaggedText(c *TaggedTextContext)

	// ExitLookup is called when exiting the lookup production.
	ExitLookup(c *LookupContext)

	// ExitLdp is called when exiting the ldp production.
	ExitLdp(c *LdpContext)

	// ExitInsertBreak is called when exiting the insertBreak production.
	ExitInsertBreak(c *InsertBreakContext)

	// ExitImportBlock is called when exiting the importBlock production.
	ExitImportBlock(c *ImportBlockContext)

	// ExitModeOverride is called when exiting the modeOverride production.
	ExitModeOverride(c *ModeOverrideContext)

	// ExitRole is called when exiting the role production.
	ExitRole(c *RoleContext)

	// ExitSectionFragment is called when exiting the sectionFragment production.
	ExitSectionFragment(c *SectionFragmentContext)

	// ExitTemplateFragment is called when exiting the templateFragment production.
	ExitTemplateFragment(c *TemplateFragmentContext)

	// ExitTemplateStatus is called when exiting the templateStatus production.
	ExitTemplateStatus(c *TemplateStatusContext)

	// ExitQualifiedName is called when exiting the qualifiedName production.
	ExitQualifiedName(c *QualifiedNameContext)

	// ExitQualifiedNameWithWildCard is called when exiting the qualifiedNameWithWildCard production.
	ExitQualifiedNameWithWildCard(c *QualifiedNameWithWildCardContext)

	// ExitLdpType is called when exiting the ldpType production.
	ExitLdpType(c *LdpTypeContext)

	// ExitBreakType is called when exiting the breakType production.
	ExitBreakType(c *BreakTypeContext)

	// ExitLanguage is called when exiting the language production.
	ExitLanguage(c *LanguageContext)

	// ExitDayOfWeek is called when exiting the dayOfWeek production.
	ExitDayOfWeek(c *DayOfWeekContext)

	// ExitModeTypes is called when exiting the modeTypes production.
	ExitModeTypes(c *ModeTypesContext)

	// ExitMonthName is called when exiting the monthName production.
	ExitMonthName(c *MonthNameContext)

	// ExitTemplateStatuses is called when exiting the templateStatuses production.
	ExitTemplateStatuses(c *TemplateStatusesContext)
}
