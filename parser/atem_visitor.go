// Code generated from java-escape by ANTLR 4.11.1. DO NOT EDIT.

package parser // Atem

import "github.com/antlr/antlr4/runtime/Go/antlr/v4"

// A complete Visitor for a parse tree produced by AtemParser.
type AtemVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by AtemParser#atemModel.
	VisitAtemModel(ctx *AtemModelContext) interface{}

	// Visit a parse tree produced by AtemParser#head.
	VisitHead(ctx *HeadContext) interface{}

	// Visit a parse tree produced by AtemParser#headComponent.
	VisitHeadComponent(ctx *HeadComponentContext) interface{}

	// Visit a parse tree produced by AtemParser#commemoration.
	VisitCommemoration(ctx *CommemorationContext) interface{}

	// Visit a parse tree produced by AtemParser#preface.
	VisitPreface(ctx *PrefaceContext) interface{}

	// Visit a parse tree produced by AtemParser#prefaceElementType.
	VisitPrefaceElementType(ctx *PrefaceElementTypeContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterFragment.
	VisitHeaderFooterFragment(ctx *HeaderFooterFragmentContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterText.
	VisitHeaderFooterText(ctx *HeaderFooterTextContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterDate.
	VisitHeaderFooterDate(ctx *HeaderFooterDateContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterPageNumber.
	VisitHeaderFooterPageNumber(ctx *HeaderFooterPageNumberContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterLookup.
	VisitHeaderFooterLookup(ctx *HeaderFooterLookupContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterTitle.
	VisitHeaderFooterTitle(ctx *HeaderFooterTitleContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterCommemoration.
	VisitHeaderFooterCommemoration(ctx *HeaderFooterCommemorationContext) interface{}

	// Visit a parse tree produced by AtemParser#setDate.
	VisitSetDate(ctx *SetDateContext) interface{}

	// Visit a parse tree produced by AtemParser#mcDay.
	VisitMcDay(ctx *McDayContext) interface{}

	// Visit a parse tree produced by AtemParser#templateTitle.
	VisitTemplateTitle(ctx *TemplateTitleContext) interface{}

	// Visit a parse tree produced by AtemParser#pageKeepWithNext.
	VisitPageKeepWithNext(ctx *PageKeepWithNextContext) interface{}

	// Visit a parse tree produced by AtemParser#pageHeaderEven.
	VisitPageHeaderEven(ctx *PageHeaderEvenContext) interface{}

	// Visit a parse tree produced by AtemParser#pageHeaderOdd.
	VisitPageHeaderOdd(ctx *PageHeaderOddContext) interface{}

	// Visit a parse tree produced by AtemParser#pageFooterEven.
	VisitPageFooterEven(ctx *PageFooterEvenContext) interface{}

	// Visit a parse tree produced by AtemParser#pageFooterOdd.
	VisitPageFooterOdd(ctx *PageFooterOddContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterColumn.
	VisitHeaderFooterColumn(ctx *HeaderFooterColumnContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterColumnLeft.
	VisitHeaderFooterColumnLeft(ctx *HeaderFooterColumnLeftContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterColumnCenter.
	VisitHeaderFooterColumnCenter(ctx *HeaderFooterColumnCenterContext) interface{}

	// Visit a parse tree produced by AtemParser#headerFooterColumnRight.
	VisitHeaderFooterColumnRight(ctx *HeaderFooterColumnRightContext) interface{}

	// Visit a parse tree produced by AtemParser#pageNumber.
	VisitPageNumber(ctx *PageNumberContext) interface{}

	// Visit a parse tree produced by AtemParser#abstractComponent.
	VisitAbstractComponent(ctx *AbstractComponentContext) interface{}

	// Visit a parse tree produced by AtemParser#actor.
	VisitActor(ctx *ActorContext) interface{}

	// Visit a parse tree produced by AtemParser#block.
	VisitBlock(ctx *BlockContext) interface{}

	// Visit a parse tree produced by AtemParser#dialog.
	VisitDialog(ctx *DialogContext) interface{}

	// Visit a parse tree produced by AtemParser#hymn.
	VisitHymn(ctx *HymnContext) interface{}

	// Visit a parse tree produced by AtemParser#media.
	VisitMedia(ctx *MediaContext) interface{}

	// Visit a parse tree produced by AtemParser#paragraph.
	VisitParagraph(ctx *ParagraphContext) interface{}

	// Visit a parse tree produced by AtemParser#passThroughHtml.
	VisitPassThroughHtml(ctx *PassThroughHtmlContext) interface{}

	// Visit a parse tree produced by AtemParser#reading.
	VisitReading(ctx *ReadingContext) interface{}

	// Visit a parse tree produced by AtemParser#restoreLocale.
	VisitRestoreLocale(ctx *RestoreLocaleContext) interface{}

	// Visit a parse tree produced by AtemParser#rubric.
	VisitRubric(ctx *RubricContext) interface{}

	// Visit a parse tree produced by AtemParser#section.
	VisitSection(ctx *SectionContext) interface{}

	// Visit a parse tree produced by AtemParser#setLocale.
	VisitSetLocale(ctx *SetLocaleContext) interface{}

	// Visit a parse tree produced by AtemParser#subTitle.
	VisitSubTitle(ctx *SubTitleContext) interface{}

	// Visit a parse tree produced by AtemParser#title.
	VisitTitle(ctx *TitleContext) interface{}

	// Visit a parse tree produced by AtemParser#verse.
	VisitVerse(ctx *VerseContext) interface{}

	// Visit a parse tree produced by AtemParser#version.
	VisitVersion(ctx *VersionContext) interface{}

	// Visit a parse tree produced by AtemParser#versionSwitch.
	VisitVersionSwitch(ctx *VersionSwitchContext) interface{}

	// Visit a parse tree produced by AtemParser#versionSwitchType.
	VisitVersionSwitchType(ctx *VersionSwitchTypeContext) interface{}

	// Visit a parse tree produced by AtemParser#whenDate.
	VisitWhenDate(ctx *WhenDateContext) interface{}

	// Visit a parse tree produced by AtemParser#whenDateCase.
	VisitWhenDateCase(ctx *WhenDateCaseContext) interface{}

	// Visit a parse tree produced by AtemParser#whenOther.
	VisitWhenOther(ctx *WhenOtherContext) interface{}

	// Visit a parse tree produced by AtemParser#whenPeriodCase.
	VisitWhenPeriodCase(ctx *WhenPeriodCaseContext) interface{}

	// Visit a parse tree produced by AtemParser#abstractDayCase.
	VisitAbstractDayCase(ctx *AbstractDayCaseContext) interface{}

	// Visit a parse tree produced by AtemParser#dayRange.
	VisitDayRange(ctx *DayRangeContext) interface{}

	// Visit a parse tree produced by AtemParser#daySet.
	VisitDaySet(ctx *DaySetContext) interface{}

	// Visit a parse tree produced by AtemParser#whenMovableCycleDay.
	VisitWhenMovableCycleDay(ctx *WhenMovableCycleDayContext) interface{}

	// Visit a parse tree produced by AtemParser#abstractDateCase.
	VisitAbstractDateCase(ctx *AbstractDateCaseContext) interface{}

	// Visit a parse tree produced by AtemParser#dateRange.
	VisitDateRange(ctx *DateRangeContext) interface{}

	// Visit a parse tree produced by AtemParser#dateSet.
	VisitDateSet(ctx *DateSetContext) interface{}

	// Visit a parse tree produced by AtemParser#whenDayName.
	VisitWhenDayName(ctx *WhenDayNameContext) interface{}

	// Visit a parse tree produced by AtemParser#whenDayNameCase.
	VisitWhenDayNameCase(ctx *WhenDayNameCaseContext) interface{}

	// Visit a parse tree produced by AtemParser#abstractDayNameCase.
	VisitAbstractDayNameCase(ctx *AbstractDayNameCaseContext) interface{}

	// Visit a parse tree produced by AtemParser#dayNameRange.
	VisitDayNameRange(ctx *DayNameRangeContext) interface{}

	// Visit a parse tree produced by AtemParser#dayNameSet.
	VisitDayNameSet(ctx *DayNameSetContext) interface{}

	// Visit a parse tree produced by AtemParser#whenExists.
	VisitWhenExists(ctx *WhenExistsContext) interface{}

	// Visit a parse tree produced by AtemParser#whenExistsCase.
	VisitWhenExistsCase(ctx *WhenExistsCaseContext) interface{}

	// Visit a parse tree produced by AtemParser#whenModeOfWeek.
	VisitWhenModeOfWeek(ctx *WhenModeOfWeekContext) interface{}

	// Visit a parse tree produced by AtemParser#whenModeOfWeekCase.
	VisitWhenModeOfWeekCase(ctx *WhenModeOfWeekCaseContext) interface{}

	// Visit a parse tree produced by AtemParser#modeOfWeekSet.
	VisitModeOfWeekSet(ctx *ModeOfWeekSetContext) interface{}

	// Visit a parse tree produced by AtemParser#whenLukanCycleDay.
	VisitWhenLukanCycleDay(ctx *WhenLukanCycleDayContext) interface{}

	// Visit a parse tree produced by AtemParser#whenSundayAfterElevationOfCrossDay.
	VisitWhenSundayAfterElevationOfCrossDay(ctx *WhenSundayAfterElevationOfCrossDayContext) interface{}

	// Visit a parse tree produced by AtemParser#whenSundaysBeforeTriodion.
	VisitWhenSundaysBeforeTriodion(ctx *WhenSundaysBeforeTriodionContext) interface{}

	// Visit a parse tree produced by AtemParser#sundaysBeforeTriodionCase.
	VisitSundaysBeforeTriodionCase(ctx *SundaysBeforeTriodionCaseContext) interface{}

	// Visit a parse tree produced by AtemParser#dayOverride.
	VisitDayOverride(ctx *DayOverrideContext) interface{}

	// Visit a parse tree produced by AtemParser#elementType.
	VisitElementType(ctx *ElementTypeContext) interface{}

	// Visit a parse tree produced by AtemParser#resourceText.
	VisitResourceText(ctx *ResourceTextContext) interface{}

	// Visit a parse tree produced by AtemParser#taggedText.
	VisitTaggedText(ctx *TaggedTextContext) interface{}

	// Visit a parse tree produced by AtemParser#lookup.
	VisitLookup(ctx *LookupContext) interface{}

	// Visit a parse tree produced by AtemParser#ldp.
	VisitLdp(ctx *LdpContext) interface{}

	// Visit a parse tree produced by AtemParser#insertBreak.
	VisitInsertBreak(ctx *InsertBreakContext) interface{}

	// Visit a parse tree produced by AtemParser#importBlock.
	VisitImportBlock(ctx *ImportBlockContext) interface{}

	// Visit a parse tree produced by AtemParser#modeOverride.
	VisitModeOverride(ctx *ModeOverrideContext) interface{}

	// Visit a parse tree produced by AtemParser#role.
	VisitRole(ctx *RoleContext) interface{}

	// Visit a parse tree produced by AtemParser#sectionFragment.
	VisitSectionFragment(ctx *SectionFragmentContext) interface{}

	// Visit a parse tree produced by AtemParser#templateFragment.
	VisitTemplateFragment(ctx *TemplateFragmentContext) interface{}

	// Visit a parse tree produced by AtemParser#templateStatus.
	VisitTemplateStatus(ctx *TemplateStatusContext) interface{}

	// Visit a parse tree produced by AtemParser#qualifiedName.
	VisitQualifiedName(ctx *QualifiedNameContext) interface{}

	// Visit a parse tree produced by AtemParser#qualifiedNameWithWildCard.
	VisitQualifiedNameWithWildCard(ctx *QualifiedNameWithWildCardContext) interface{}

	// Visit a parse tree produced by AtemParser#ldpType.
	VisitLdpType(ctx *LdpTypeContext) interface{}

	// Visit a parse tree produced by AtemParser#breakType.
	VisitBreakType(ctx *BreakTypeContext) interface{}

	// Visit a parse tree produced by AtemParser#language.
	VisitLanguage(ctx *LanguageContext) interface{}

	// Visit a parse tree produced by AtemParser#dayOfWeek.
	VisitDayOfWeek(ctx *DayOfWeekContext) interface{}

	// Visit a parse tree produced by AtemParser#modeTypes.
	VisitModeTypes(ctx *ModeTypesContext) interface{}

	// Visit a parse tree produced by AtemParser#monthName.
	VisitMonthName(ctx *MonthNameContext) interface{}

	// Visit a parse tree produced by AtemParser#templateStatuses.
	VisitTemplateStatuses(ctx *TemplateStatusesContext) interface{}
}
